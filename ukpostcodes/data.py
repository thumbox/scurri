import pickle


class OSOpenDataCodePointData:
    """ OSOpenDataCodePoint

    A serialized version of Code-Point Open Linked Data csv files.
    It returns a set of all valid and active postal codes.
    """

    GEOCODE_DB = None

    @classmethod
    def get(cls):

        if not cls.GEOCODE_DB:
            with open('postcodes.pkl', 'rb') as db:
                cls.GEOCODE_DB = pickle.load(db)

        return cls.GEOCODE_DB