"""
UK Postcodes

Validating and formatting post codes for UK.
"""

import re
from six import string_types
from data import OSOpenDataCodePointData


class UKPostcode:

    RE_GEOCODES = [
        re.compile(r'^(([Gg][Ii])([Rr]))\s?((0)([Aa]{2}))$'),
        re.compile(r'^(([A-Za-z])([0-9]{1,2}))\s?(([0-9])([A-Za-z]{2}))$'),
        re.compile(r'^(([A-Za-z][A-Ha-hJ-Yj-y])([0-9]{1,2}))\s?(([0-9])([A-Za-z]{2}))$'),
        re.compile(r'^(([A-Za-z])([0-9][A-Za-z]))\s?(([0-9])([A-Za-z]{2}))$'),
        re.compile(r'^(([A-Za-z][A-Ha-hJ-Yj-y])([0-9]?[A-Za-z]))\s?(([0-9])([A-Za-z]{2}))$')
    ]

    def __init__(self, postcode):

        self.outward = None
        self.inward = None
        self.area = None
        self.district = None
        self.sector = None
        self.unit = None

        self._db = OSOpenDataCodePointData.get()
        self._parse(postcode)

    def _normalize(self, postcode):

        if not isinstance(postcode, string_types):
            raise ValueError('Postcode must be a string.')

        code = postcode.replace(' ', '')
        code = code.strip()
        code = code.upper()
        return code

    def _parse(self, postcode):

        code = self._normalize(postcode)

        for regex in self.RE_GEOCODES:
            match = regex.match(code)
            if match:
                outward, area, district, inward, sector, _ = match.groups()
                self.outward = outward
                self.inward = inward
                self.area = area
                self.district = '{0}{1}'.format(area, district)
                self.sector = '{0}{1} {2}'.format(area, district, sector)
                self.unit = '{0} {1}'.format(outward, inward)

                if code in self._db:
                    return

        raise ValueError('Invalid UK Postcode: %s' % postcode)
