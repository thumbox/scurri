"""
UKPostcode TestCases
"""
import unittest
import collections

from ukpostcode import UKPostcode


class UKPostcodeTestCase(unittest.TestCase):

    def test_table_valid_formats(self):

        Postcode = collections.namedtuple(
            'Postcode', 'code outward inward area district sector unit'
        )

        codes = [
            Postcode('TW8 9GS', 'TW8', '9GS', 'TW', 'TW8', 'TW8 9', 'TW8 9GS'),
            Postcode('CR2 6XH', 'CR2', '6XH', 'CR', 'CR2', 'CR2 6', 'CR2 6XH'),
            Postcode('DN55 1PT', 'DN55', '1PT', 'DN','DN55', 'DN55 1', 'DN55 1PT'),
            Postcode('M1 1AE', 'M1', '1AE', 'M', 'M1', 'M1 1', 'M1 1AE'),
            Postcode('GIR 0AA', 'GIR', '0AA', 'GI', 'GIR', 'GIR 0', 'GIR 0AA'),
            Postcode('B33 8TH', 'B33', '8TH', 'B', 'B33', 'B33 8', 'B33 8TH'),
            Postcode('EC1A 1BB', 'EC1A', '1BB', 'EC','EC1A', 'EC1A 1', 'EC1A 1BB'),
            Postcode('M11AE', 'M1', '1AE', 'M', 'M1', 'M1 1', 'M1 1AE'),
        ]

        for expected in codes:
            actual = UKPostcode(expected.code)
            self.assertEqual(actual.outward, expected.outward)
            self.assertEqual(actual.inward, expected.inward)
            self.assertEqual(actual.area, expected.area)
            self.assertEqual(actual.district, expected.district)
            self.assertEqual(actual.sector, expected.sector)
            self.assertEqual(actual.unit, expected.unit)

    def test_table_invalid_formats(self):

        codes = [
            'EC1A BBB',
            '123',
            'GIG 0AA',
            'BFPO 569',
            'AB1 AA1',
            'W1A0AX',
            None,
            123
        ]

        for invalid in codes:
            with self.assertRaises(ValueError):
                UKPostcode(invalid)
