"""
ThreeFive

Prints the numbers from a given range.
For multiples of three print 'Three' instead of the number.
For multiples of five print 'Five' instead of the number.
For numbers which are multiples of both three and five print "ThreeFive".

"""


def three_five(numbers):

    for number in numbers:
        if number % 15 == 0:
            yield "TheeFive"
        elif number % 5 == 0:
            yield "Five"
        elif number % 3 == 0:
            yield "Three"
        else:
            yield number
