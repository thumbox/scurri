"""
ThreeFive TestCases
"""
import types
import unittest

from threefive import three_five


class ThreeFiveTest(unittest.TestCase):

    def test_from_0_to_15(self):

        expected = [
            'TheeFive', 1, 2, 'Three', 4, 'Five', 'Three', 7, 8,
            'Three', 'Five', 11, 'Three', 13, 14, 'TheeFive'
        ]
        actual = list(three_five(range(0, 16)))
        self.assertEqual(expected, actual)

    def test_negative_range(self):

        expected = [
            'TheeFive', -14, -13, 'Three', -11, 'Five', 'Three', -8,
            -7, 'Three', 'Five', -4, 'Three', -2, -1, 'TheeFive'
        ]
        actual = list(three_five(range(-15, 1)))
        self.assertEqual(expected, actual)

    def test_empty_range(self):

        expected = []
        actual = list(three_five([]))
        self.assertEqual(expected, actual)

    def test_return_type(self):

        expected = type(three_five([]))
        actual = types.GeneratorType
        self.assertEqual(expected, actual)

    def test_raise_if_not_iterable(self):

        with self.assertRaises(TypeError) as context:
            next(three_five(None))
        self.assertIn('object is not iterable', context.exception.message)
